Que1 - Please Write a java code inside the method 'CapitalizeFirstLetter(String s) Please implement this method to Capitalize all first letters of the word in the given String. all other symbols shall intact. if a word starts not with a letter, it shall  remain intact too. assume that the parameter String can only contain spaces and alphanumerics characters

Note - Please keep in mind that the words can we divided by single or multiple spaces. the spaces also can be found at beginning or the end of the parameter string and you need to preserve them

Output -- Input                      Output
          {"hello world "}  --       Hello word
          {"aditya pramod rao"} ---  Aditya Pramod Rao


Que2 - Please write java code inside the method 'retainPositiveNumber(int[] a)', Please implement this method to retain a new array with only positive number from the given array. the elements in the resulting array shall be sorted in the ascending order.

Output -- input                           Output
          {10,16,-16,18,-5,17} =====      {10,17,18,22}


Que3 - Please Write a java code inside the method 'getSumOfNumbers(String s)', Please implement this method to return the sum of all integers found in the parameter String. you can assume that integers are separated form other parts with one or more spaces('' symbols)  
Output --
     input                                        Output
     ("20 30")                                        50
     ("5 sad")                                         5
     ("10 asgd 20 shd 30")                             60
     (null)                                        Handle Exception

Que4 - Please Write a java code inside the method 'getRequiredNumberOfBits(int N)', Please implement this method to return the number of bits which is just enough to store any integers form 0 to N-1 inclusivly form any int N > 0
Output --

      input      Output
       1           1
       16           5
       100         7
       5           3
       32          6
